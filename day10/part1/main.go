package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"sort"
	"strings"
)

type SpaceByDistance struct {
	Spaces []Space
	Center Space
}

func (s SpaceByDistance) Len() int {
	return len(s.Spaces)
}

func (s SpaceByDistance) Less(i int, j int) bool {
	return s.Center.Distance(s.Spaces[i]) < s.Center.Distance(s.Spaces[j])
}

func (s SpaceByDistance) Swap(i int, j int) {
	s.Spaces[i], s.Spaces[j] = s.Spaces[j], s.Spaces[i]
}

type Space struct {
	S       string
	Col     int
	Row     int
	Checked bool
}

func (s Space) Distance(s2 Space) int {
	return abs(s.Row-s2.Row) + abs(s.Col-s2.Col)
}

func main() {
	input := readInput()

	r, c, detected := findMostSeen(input)
	fmt.Printf("%d, %d sees %d\n", c, r, detected)
}

func findMostSeen(input [][]Space) (int, int, int) {
	mostSeen := 0
	mostSeenSpace := Space{}

	asteroids := make([]Space, 0)

	for r := 0; r < len(input); r++ {
		row := input[r]
		for c := 0; c < len(row); c++ {
			if row[c].S == "#" {
				asteroids = append(asteroids, row[c])
			}
		}
	}

	for _, space := range asteroids {
		numSeen := len(space.asteroidsSeen(asteroids))
		if numSeen > mostSeen {
			mostSeenSpace = space
			mostSeen = numSeen
		}
		fmt.Printf("(%d, %d) sees %v\n", space.Col, space.Row, numSeen)
	}

	return mostSeenSpace.Row, mostSeenSpace.Col, mostSeen
}

func (center Space) asteroidsSeen(asteroids []Space) []Space {
	canSee := make([]Space, 0)

	tmp := make([]Space, len(asteroids))
	copy(tmp, asteroids)

	// sort asteroids by distance to center
	sbd := SpaceByDistance{
		Spaces: tmp,
		Center: center,
	}

	sort.Sort(sbd)

	// in sorted order
	for _, a := range sbd.Spaces {
		if a.Col == center.Col && a.Row == center.Row {
			continue
		}

		// check to see if the asteroid is an even multiple of anything already seen
		shadowed := false
		for _, seen := range canSee {
			if center.EvenMultiple(seen, a) {
				shadowed = true
				break
			}
		}

		if !shadowed {
			canSee = append(canSee, a)
			// fmt.Printf("%v is not blocked\n\n", a)
		} else {
			// fmt.Printf("%v is blocked\n\n", a)
		}
	}

	return canSee
}

func (center Space) EvenMultiple(a Space, b Space) bool {
	// this is broken
	xDeltaA := a.Col - center.Col
	yDeltaA := a.Row - center.Row

	// fmt.Printf("Checking if %v blocks %v from %v\n", a, b, center)
	if xDeltaA == 0 && yDeltaA == 0 {
		// fmt.Println("Same spot, don't count it")
		return true
	}

	gcd := abs(gcd(xDeltaA, yDeltaA))
	xDeltaA = xDeltaA / gcd
	yDeltaA = yDeltaA / gcd

	xDeltaB := b.Col - center.Col
	yDeltaB := b.Row - center.Row

	for i := 0; i < max(abs(xDeltaB), abs(yDeltaB))+2; i++ {
		x := xDeltaA * i
		y := yDeltaA * i
		// fmt.Printf("(%d, %d) against (%d, %d) with %d\n", x, y, xDeltaB, yDeltaB, i)
		if x == xDeltaB && y == yDeltaB {
			// fmt.Printf("(%d, %d) blocks (%d, %d) when viewed from (%d, %d)\n",
			// 	a.Col, a.Row, b.Col, b.Row, center.Col, center.Row)
			return true
		}
	}

	// fmt.Println("Unobstructed")
	return false
}

func readInput() [][]Space {
	data, err := ioutil.ReadFile("input.txt")
	if err != nil {
		log.Fatal(err)
	}

	dataStrings := strings.Split(string(data), "\n")
	result := make([][]Space, len(dataStrings))

	for i, line := range dataStrings {
		row := make([]Space, len(line))

		for j, r := range line {
			row[j].S = string(r)
			row[j].Checked = false
			row[j].Row = i
			row[j].Col = j
		}

		result[i] = row
	}

	return result
}

func abs(i int) int {
	if i < 0 {
		return -i
	}
	return i
}

func max(a int, b int) int {
	if a > b {
		return a
	}
	return b
}

func gcd(a int, b int) int {
	for b != 0 {
		t := b
		b = a % b
		a = t
	}
	return a
}
