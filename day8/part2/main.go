package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strconv"
	"strings"
)

const (
	Width  = 25
	Height = 6
)

type Layer []int

func main() {
	input := readInput()
	layers := makeLayers(input, Width, Height)
	image := stackLayers(layers, Width, Height)
	printLayer(image, Width, Height)
}

func stackLayers(layers []Layer, width int, height int) Layer {
	result := make(Layer, width*height)

	for h := 0; h < height; h++ {
		for w := 0; w < width; w++ {
			index := h*width + w

			for _, layer := range layers {
				if layer[index] == 2 {
					continue
				}

				result[index] = layer[index]
				break
			}
		}
	}

	return result
}

func printLayer(layer Layer, width int, height int) {
	for h := 0; h < height; h++ {
		for w := 0; w < width; w++ {
			fmt.Printf("%d", layer[h*width+w])
		}
		fmt.Println()
	}
}

func makeLayers(input []int, width int, height int) []Layer {
	layers := make([]Layer, 0)

	for j := 0; j < len(input); j += (width * height) {
		layer := make(Layer, width*height)
		for i := 0; i < width*height; i++ {
			layer[i] = input[i+j]
		}
		layers = append(layers, layer)
	}

	return layers
}

func readInput() []int {
	raw, err := ioutil.ReadFile("input.txt")
	if err != nil {
		log.Fatal("Could not read input, ", err)
	}

	line := strings.Split(string(raw), "\n")[0]
	ints := make([]int, len(line))

	for i, r := range line {
		ints[i], err = strconv.Atoi(string(r))
		if err != nil {
			log.Fatal("Could not parse %d: %s", i, r)
		}
	}

	return ints
}
