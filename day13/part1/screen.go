package main

type Row map[int]int
type Screen struct {
	Rows map[int]Row
}

func NewScreen() Screen {
	return Screen{
		Rows: make(map[int]Row),
	}
}

func (s *Screen) Write(x int, y int, value int) {
	_, exists := s.Rows[y]
	if !exists {
		s.Rows[y] = make(Row)
		s.Rows[y][x] = value
		return
	}

	_, exists = s.Rows[y][x]
	if !exists {
		s.Rows[y][x] = value
		return
	}

	s.Rows[y][x] = value
	// 0 empty
	// 1 wall (indestructable)
	// 2 block (breakable by ball)
	// 3 paddle (indestructable)
	// 4 ball
	// oldValue := s[y][x]
}

func (s Screen) Count() map[int]int {
	results := make(map[int]int)

	for _, row := range s.Rows {
		for _, val := range row {
			count, exists := results[val]
			if !exists {
				count = 0
			}

			results[val] = count + 1
		}
	}

	return results
}
