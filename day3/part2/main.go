package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strconv"
	"strings"
)

type InstructionSet []string
type GridPoint struct {
	X int
	Y int
}

func (g GridPoint) ManhattanDistance() int {
	return abs(g.X) + abs(g.Y)
}

func abs(i int) int {
	if i < 0 {
		return -i
	}
	return i
}

type Line struct {
	Start GridPoint
	End   GridPoint
}

func (line Line) Length() int {
	return abs(line.Start.X-line.End.X) + abs(line.Start.Y-line.End.Y)
}

func (line Line) ContainsPoint(g GridPoint) bool {
	if line.Start.X == line.End.X {
		return g.X == line.Start.X && Overlap(line.Start.Y, line.End.Y, g.Y)
	}
	return g.Y == line.Start.Y && Overlap(line.Start.X, line.End.X, g.X)
}

func (line Line) DistanceToPoint(g GridPoint) (int, error) {
	if !line.ContainsPoint(g) {
		return 0, fmt.Errorf("Does not contain point")
	}

	deltaX := abs(line.Start.X - g.X)
	deltaY := abs(line.Start.Y - g.Y)
	return deltaX + deltaY, nil
}

func main() {
	instructionSets := readInput()

	// intersections := make([]GridPoint, 0)
	existingLines := make([]Line, 0)

	shortestDistance := 99999999

	for _, instructionSet := range instructionSets {
		newLines := runInstructions(instructionSet)

		for _, line := range existingLines {
			for _, newLine := range newLines {
				intersection, err := line.Intersects(newLine)
				if err != nil {
					continue
				}

				if intersection.X == 0 && intersection.Y == 0 {
					continue
				}

				fmt.Printf("%v is an intersection on %v and %v\n", intersection, line, newLine)

				firstDistance, err := distanceToPoint(existingLines, intersection)
				if err != nil {
					log.Fatal("Point not in lines, error")
				}

				secondDistance, err := distanceToPoint(newLines, intersection)
				if err != nil {
					log.Fatal("Point not in lines, error")
				}

				distance := firstDistance + secondDistance
				if distance < shortestDistance {
					shortestDistance = distance
				}
			}
		}

		existingLines = append(existingLines, newLines...)
	}

	fmt.Println(shortestDistance)
}

func distanceToPoint(lines []Line, point GridPoint) (int, error) {
	distance := 0

	for _, line := range lines {
		if line.ContainsPoint(point) {
			r, err := line.DistanceToPoint(point)
			if err != nil {
				log.Fatal("asdf")
			}
			distance += r
			return distance, nil
		}

		distance += line.Length()
	}

	return 0, fmt.Errorf("Point not in lines")
}

func runInstructions(instructions InstructionSet) []Line {
	start := GridPoint{0, 0}

	lines := make([]Line, len(instructions))

	for i, instruction := range instructions {
		if len(instruction) == 0 {
			continue
		}

		direction := instruction[0]
		amount, err := strconv.Atoi(instruction[1:len(instruction)])
		if err != nil {
			log.Fatal("Could not find amount on %s", instruction)
		}

		end := start

		switch direction {
		case 'U':
			end.Y += amount
		case 'D':
			end.Y -= amount
		case 'L':
			end.X -= amount
		case 'R':
			end.X += amount
		default:
			log.Fatal("Unknown distance")
		}

		line := Line{
			Start: start,
			End:   end,
		}

		// fmt.Printf("Moved %v to %v\n", start, end)
		fmt.Printf("Line %v\n", line)

		lines[i] = line
		start = end
	}

	return lines
}

func (l Line) Intersects(l2 Line) (GridPoint, error) {
	intersection := GridPoint{}
	// parallel lines never intersect
	lHorizontal := l.Start.Y == l.End.Y
	l2Horizontal := l2.Start.Y == l2.End.Y

	if lHorizontal == l2Horizontal {
		return intersection, fmt.Errorf("No intersection, parallel")
	}

	horizontal := l2
	vertical := l

	if lHorizontal {
		horizontal = l
		vertical = l2
	}

	// check vertical X between horizontal line
	if Overlap(horizontal.Start.X, horizontal.End.X, vertical.Start.X) {
		// check horizontal Y between vertical line
		if Overlap(vertical.Start.Y, vertical.End.Y, horizontal.Start.Y) {
			intersection.X = vertical.Start.X
			intersection.Y = horizontal.Start.Y

			return intersection, nil
		}
	}
	return intersection, fmt.Errorf("No intersection")
}

func Overlap(start int, end int, test int) bool {
	if start > end {
		temp := start
		start = end
		end = temp
	}
	return start <= test && test <= end
}

func readInput() []InstructionSet {
	raw, err := ioutil.ReadFile("input.txt")
	if err != nil {
		log.Fatal("Could not read input, ", err)
	}

	lines := strings.Split(string(raw), "\n")
	sets := make([]InstructionSet, len(lines))

	for i, line := range lines {
		sets[i] = strings.Split(line, ",")
	}

	return sets
}
