package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strconv"
	"strings"
)

const (
	ADD = 1
	MUL = 2
	END = 99
)

func main() {
	inputs := readInput()

	// process([]int{1, 0, 0, 0, 99})
	// process([]int{2, 3, 0, 3, 99})
	// process([]int{2, 4, 4, 5, 99, 0})
	// process([]int{1, 1, 1, 4, 99, 5, 6, 0, 99})

	// inputs[1] = 12
	// inputs[2] = 2
	for i := 0; i < len(inputs); i++ {
		for j := 0; j < len(inputs); j++ {
			inputCopy := make([]int, len(inputs))
			copy(inputCopy, inputs)
			if process(inputCopy, i, j, 19690720) {
				fmt.Printf("Noun: %d Verb: %d\n", i, j)
				fmt.Println(inputCopy[0])
				fmt.Printf("Result: %d\n", i*100+j)
			}
		}
	}
}

func process(inputs []int, noun int, verb int, target int) bool {
	inputs[1] = noun
	inputs[2] = verb
	programCounter := 0
	for inputs[programCounter] != END {
		switch inputs[programCounter] {
		case ADD:
			op1 := inputs[programCounter+1]
			op2 := inputs[programCounter+2]
			dest := inputs[programCounter+3]

			inputs[dest] = inputs[op1] + inputs[op2]

			programCounter += 4
		case MUL:
			op1 := inputs[programCounter+1]
			op2 := inputs[programCounter+2]
			dest := inputs[programCounter+3]

			inputs[dest] = inputs[op1] * inputs[op2]

			programCounter += 4

		default:
			log.Fatal("Got invalid instruction at %d: %d", programCounter, inputs[programCounter])
		}
	}
	// fmt.Printf("%v\n", inputs)
	// fmt.Println(inputs[0])
	// fmt.Println()

	return inputs[0] == target
}

func readInput() []int {
	raw, err := ioutil.ReadFile("input.txt")
	if err != nil {
		log.Fatal("Could not read input, ", err)
	}

	line := strings.Split(string(raw), "\n")[0]
	pieces := strings.Split(line, ",")

	ints := make([]int, len(pieces))
	for i, s := range pieces {
		ints[i], err = strconv.Atoi(s)
		if err != nil {
			log.Fatal("Could not parse %d: %s", i, s)
		}
	}

	return ints
}
