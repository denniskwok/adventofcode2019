package main

import (
	"fmt"
)

type State struct {
	Steps int
	Moons []Moon
}

func (s *State) TakeStep() {
	s.Steps++

	// apply gravity
	for i := range s.Moons {
		for _, m2 := range s.Moons {
			// fmt.Printf("pre G %+v\n", m.Velocity)
			s.Moons[i].ApplyGravity(m2)
			// fmt.Printf("post G %+v\n", m.Velocity)
		}
	}

	// apply velocity
	for i := range s.Moons {
		// fmt.Printf("pre V %+v\n", m.Position)
		s.Moons[i].ApplyVelocity()
		// fmt.Printf("post V %+v\n", m.Position)
	}
}

func (s State) TotalEnergy() int {
	sum := 0
	for _, m := range s.Moons {
		sum += m.TotalEnergy()
	}
	return sum
}

func (s State) Print() {
	fmt.Printf("Step %d: Total Energy: %d\n", s.Steps, s.TotalEnergy())
	for _, m := range s.Moons {
		fmt.Printf("\t%d pos: %+v vel: %+v\n", m.Number, m.Position, m.Velocity)
	}
}
