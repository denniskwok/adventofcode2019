package main

import (
	"io/ioutil"
	"log"
	"strconv"
	"strings"
)

func main() {
	s := readInput()

	for i := 0; i < 1001; i++ {
		if i%100 == 0 {
			s.Print()
		}
		s.TakeStep()
	}
}

func readInput() State {
	// <x=-3, y=10, z=-1>
	raw, err := ioutil.ReadFile("input.txt")
	if err != nil {
		log.Fatal("Could not read input, ", err)
	}

	lines := strings.Split(string(raw), "\n")
	moons := make([]Moon, 0)

	for i, line := range lines {
		if line == "" {
			continue
		}

		// strip out non numbers and ,
		line = strings.Replace(line, "<", "", -1)
		line = strings.Replace(line, ">", "", -1)
		line = strings.Replace(line, "x", "", -1)
		line = strings.Replace(line, "=", "", -1)
		line = strings.Replace(line, "y", "", -1)
		line = strings.Replace(line, "z", "", -1)

		positions := strings.Split(line, ", ")
		x, err := strconv.Atoi(positions[0])
		if err != nil {
			log.Fatal("Could not convert ", positions[0], err)
		}

		y, err := strconv.Atoi(positions[1])
		if err != nil {
			log.Fatal("Could not convert ", positions[1], err)
		}

		z, err := strconv.Atoi(positions[2])
		if err != nil {
			log.Fatal("Could not convert ", positions[2], err)
		}

		m := NewMoon(i, x, y, z)
		moons = append(moons, m)
	}

	return State{
		Steps: 0,
		Moons: moons,
	}
}
