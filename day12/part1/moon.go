package main

type Triplet struct {
	X int
	Y int
	Z int
}

type Moon struct {
	Number   int
	Position Triplet
	Velocity Triplet
}

func NewMoon(number int, x int, y int, z int) Moon {
	return Moon{
		Number:   number,
		Position: Triplet{X: x, Y: y, Z: z},
		Velocity: Triplet{X: 0, Y: 0, Z: 0},
	}
}

func (m Moon) TotalEnergy() int {
	return m.PotentialEnergy() * m.KineticEnergy()
}

func (m Moon) PotentialEnergy() int {
	return abs(m.Position.X) + abs(m.Position.Y) + abs(m.Position.Z)
}

func (m Moon) KineticEnergy() int {
	return abs(m.Velocity.X) + abs(m.Velocity.Y) + abs(m.Velocity.Z)
}

func (m *Moon) ApplyGravity(m2 Moon) {
	if m.Position.X != m2.Position.X {
		if m.Position.X < m2.Position.X {
			// fmt.Println("x++")
			m.Velocity.X++
		} else {
			// fmt.Println("x--")
			m.Velocity.X--
		}
	}

	if m.Position.Y != m2.Position.Y {
		if m.Position.Y < m2.Position.Y {
			m.Velocity.Y++
		} else {
			m.Velocity.Y--
		}
	}

	if m.Position.Z != m2.Position.Z {
		if m.Position.Z < m2.Position.Z {
			m.Velocity.Z++
		} else {
			m.Velocity.Z--
		}
	}
}

func (m *Moon) ApplyVelocity() {
	m.Position.X += m.Velocity.X
	m.Position.Y += m.Velocity.Y
	m.Position.Z += m.Velocity.Z
}

func abs(i int) int {
	if i < 0 {
		return -i
	}
	return i
}
