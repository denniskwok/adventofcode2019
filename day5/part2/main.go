package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strconv"
	"strings"
)

const (
	ADD           = 1
	MUL           = 2
	INPUT         = 3
	OUTPUT        = 4
	JUMP_IF_TRUE  = 5
	JUMP_IF_FALSE = 6
	LESS_THAN     = 7
	EQUAL         = 8
	END           = 99
)

func main() {
	inputs := readInput()

	process([]int{3, 9, 8, 9, 10, 9, 4, 9, 99, -1, 8})
	process([]int{3, 9, 7, 9, 10, 9, 4, 9, 99, -1, 8})
	process([]int{3, 3, 1108, -1, 8, 3, 4, 3, 99})
	process([]int{3, 3, 1107, -1, 8, 3, 4, 3, 99})

	process(inputs)
}

func fetchInput() int {
	return 5
}

func process(inputs []int) {
	programCounter := 0
	for inputs[programCounter] != END {
		opcode, immediateFirst, immediateSecond, immediateThird := decode(inputs[programCounter])
		fmt.Printf("PC: %d INST: %d opcode %d %v %v %v\n", programCounter, inputs[programCounter], opcode, immediateFirst, immediateSecond, immediateThird)
		if opcode == END {
			break
		}

		op1 := inputs[programCounter+1]

		val1 := op1

		if !immediateFirst {
			val1 = inputs[op1]
		}

		switch opcode {
		case ADD:
			op2 := inputs[programCounter+2]
			op3 := inputs[programCounter+3]

			val2 := op2
			val3 := op3

			if !immediateSecond {
				val2 = inputs[op2]
			}

			fmt.Printf("ADD %d %d %d\n", val1, val2, val3)

			inputs[val3] = val1 + val2

			programCounter += 4
		case MUL:
			op2 := inputs[programCounter+2]
			op3 := inputs[programCounter+3]

			val2 := op2
			val3 := op3

			fmt.Printf("MUL %d %d %d\n", val1, val2, val3)

			if !immediateSecond {
				val2 = inputs[op2]
			}

			fmt.Printf("MUL %d %d %d\n", val1, val2, val3)

			inputs[val3] = val1 * val2

			programCounter += 4

		case INPUT:
			val := fetchInput()
			inputs[op1] = val

			programCounter += 2

		case OUTPUT:
			val := inputs[op1]

			fmt.Println(val)

			programCounter += 2

		case JUMP_IF_TRUE:
			if val1 != 0 {
				op2 := inputs[programCounter+2]
				val2 := op2

				if !immediateSecond {
					val2 = inputs[op2]
				}

				programCounter = val2
			} else {
				programCounter += 3
			}

		case JUMP_IF_FALSE:
			if val1 == 0 {
				op2 := inputs[programCounter+2]
				val2 := op2

				if !immediateSecond {
					val2 = inputs[op2]
				}

				programCounter = val2
			} else {
				programCounter += 3
			}

		case LESS_THAN:
			op2 := inputs[programCounter+2]
			op3 := inputs[programCounter+3]

			val2 := op2
			val3 := op3

			if !immediateSecond {
				val2 = inputs[op2]
			}

			if val1 < val2 {
				inputs[val3] = 1
			} else {
				inputs[val3] = 0
			}

			programCounter += 4

		case EQUAL:
			op2 := inputs[programCounter+2]
			op3 := inputs[programCounter+3]

			val2 := op2
			val3 := op3

			if !immediateSecond {
				val2 = inputs[op2]
			}

			if val1 == val2 {
				inputs[val3] = 1
			} else {
				inputs[val3] = 0
			}

			programCounter += 4

		default:
			log.Fatal("Got invalid instruction at %d: %d", programCounter, inputs[programCounter])
		}
	}
	// fmt.Printf("%v\n", inputs)
	// fmt.Println(inputs[0])
	// fmt.Println()
}

func decode(instruction int) (int, bool, bool, bool) {
	opcode := instruction % 100
	first := false
	second := false
	third := false

	instruction = instruction / 100
	if instruction >= 100 {
		third = true
		instruction -= 100
	}

	if instruction >= 10 {
		second = true
		instruction -= 10
	}

	if instruction >= 1 {
		first = true
		instruction -= 1
	}

	return opcode, first, second, third
}

func readInput() []int {
	raw, err := ioutil.ReadFile("input.txt")
	if err != nil {
		log.Fatal("Could not read input, ", err)
	}

	line := strings.Split(string(raw), "\n")[0]
	pieces := strings.Split(line, ",")

	ints := make([]int, len(pieces))
	for i, s := range pieces {
		ints[i], err = strconv.Atoi(s)
		if err != nil {
			log.Fatal("Could not parse %d: %s", i, s)
		}
	}

	return ints
}
