package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strconv"
	"strings"
)

func main() {
	// Examples
	// fmt.Println(calcFuel(12))
	// fmt.Println(calcFuel(14))
	// fmt.Println(calcFuel(1969))
	// fmt.Println(calcFuel(100756))

	inputs := readInput()
	sum := 0
	for _, input := range inputs {
		result := calcFuel(input)
		fmt.Printf("%d needs %d\n", input, result)
		sum += result
	}

	fmt.Println(sum)
}

func readInput() []int {
	data, err := ioutil.ReadFile("input.txt")
	if err != nil {
		log.Fatal(err)
	}

	dataStrings := strings.Split(string(data), "\n")
	ints := make([]int, len(dataStrings))

	for i, line := range dataStrings {
		ints[i], err = strconv.Atoi(line)
		if err != nil {
			ints[i] = 0
		}
	}

	return ints
}

func calcFuel(mass int) int {
	result := (mass / 3) - 2
	if result < 0 {
		result = 0
	}
	return result
}
