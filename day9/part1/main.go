package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strconv"
	"strings"
)

const (
	ADD           = 1
	MUL           = 2
	INPUT         = 3
	OUTPUT        = 4
	JUMP_IF_TRUE  = 5
	JUMP_IF_FALSE = 6
	LESS_THAN     = 7
	EQUAL         = 8
	CHANGE_BASE   = 9
	END           = 99
)

type AddressMode int

const (
	POSITION  = 0
	IMMEDIATE = 1
	RELATIVE  = 2
)

func main() {
	inputs := readInput()

	// process(arrayToMap([]int64{3, 9, 8, 9, 10, 9, 4, 9, 99, -1, 8}))
	// process(arrayToMap([]int64{3, 9, 7, 9, 10, 9, 4, 9, 99, -1, 8}))
	// process(arrayToMap([]int64{3, 3, 1108, -1, 8, 3, 4, 3, 99}))
	// process(arrayToMap([]int64{3, 3, 1107, -1, 8, 3, 4, 3, 99}))

	// process(arrayToMap([]int64{109, 1, 204, -1, 1001, 100, 1, 100, 1008, 100, 16, 101, 1006, 101, 0, 99}))
	// process(arrayToMap([]int64{1102, 34915192, 34915192, 7, 4, 7, 99, 0}))
	// process(arrayToMap([]int64{104, 1125899906842624, 99}))
	process(inputs)
}

func fetchInput() int64 {
	return 1
}

func arrayToMap(inputs []int64) map[int]int64 {
	output := make(map[int]int64, len(inputs))
	for i, val := range inputs {
		output[i] = val
	}
	return output
}

func readValue(program map[int]int64, value int64, relativeBase int64, mode AddressMode) int64 {
	switch mode {
	case POSITION:
		lookup, found := program[int(value)]
		if !found {
			program[int(value)] = 0
			return 0
		}
		return lookup
	case IMMEDIATE:
		return value
	case RELATIVE:
		adjusted := relativeBase + value
		if adjusted < 0 {
			log.Fatal("Tried to access negative address")
		}
		return program[int(adjusted)]
	}
	log.Fatalf("Bad mode: %d", mode)
	return -1
}

func process(inputs map[int]int64) {
	programCounter := 0
	relativeBase := int64(0)
	for inputs[programCounter] != END {
		opcode, modeFirst, modeSecond, modeThird := decode(inputs[programCounter])
		fmt.Printf("PC: %d RB: %d INST: %d opcode: %d modes: %v %v %v\n",
			programCounter, relativeBase, inputs[programCounter], opcode,
			modeFirst, modeSecond, modeThird)
		if opcode == END {
			break
		}

		op1 := inputs[programCounter+1]
		val1 := readValue(inputs, op1, relativeBase, modeFirst)

		switch opcode {
		case ADD:
			op2 := inputs[programCounter+2]
			op3 := inputs[programCounter+3]

			val2 := readValue(inputs, op2, relativeBase, modeSecond)
			val3 := op3

			// fmt.Printf("ADD %d %d %d\n", val1, val2, val3)

			if modeThird == RELATIVE {
				address := relativeBase + op3
				inputs[int(address)] = val1 + val2
			} else {
				inputs[int(val3)] = val1 + val2
			}
			programCounter += 4
		case MUL:
			op2 := inputs[programCounter+2]
			op3 := inputs[programCounter+3]

			val2 := readValue(inputs, op2, relativeBase, modeSecond)
			val3 := op3

			// fmt.Printf("MUL %d %d %d\n", val1, val2, val3)

			if modeThird == RELATIVE {
				address := relativeBase + op3
				inputs[int(address)] = val1 * val2
			} else {
				inputs[int(val3)] = val1 * val2
			}

			programCounter += 4

		case INPUT:
			val := fetchInput()
			if modeFirst == RELATIVE {
				address := relativeBase + op1
				inputs[int(address)] = val

			} else {
				inputs[int(op1)] = val
			}

			programCounter += 2

		case OUTPUT:
			// fmt.Println(op1)
			// fmt.Println(val1)
			// val := inputs[int(op1)]

			fmt.Println(val1)

			programCounter += 2

		case JUMP_IF_TRUE:
			if val1 != 0 {
				op2 := inputs[programCounter+2]
				val2 := readValue(inputs, op2, relativeBase, modeSecond)

				programCounter = int(val2)
			} else {
				programCounter += 3
			}

		case JUMP_IF_FALSE:
			if val1 == 0 {
				op2 := inputs[programCounter+2]
				val2 := readValue(inputs, op2, relativeBase, modeSecond)

				programCounter = int(val2)
			} else {
				programCounter += 3
			}

		case LESS_THAN:
			op2 := inputs[programCounter+2]
			op3 := inputs[programCounter+3]

			val2 := readValue(inputs, op2, relativeBase, modeSecond)
			val3 := op3

			address := val3
			if modeThird == RELATIVE {
				address = relativeBase + op3
			}

			if val1 < val2 {
				inputs[int(address)] = 1
			} else {
				inputs[int(address)] = 0
			}

			programCounter += 4

		case EQUAL:
			op2 := inputs[programCounter+2]
			op3 := inputs[programCounter+3]

			val2 := readValue(inputs, op2, relativeBase, modeSecond)
			val3 := op3

			address := val3
			if modeThird == RELATIVE {
				address = relativeBase + op3
			}
			if val1 == val2 {
				inputs[int(address)] = 1
			} else {
				inputs[int(address)] = 0
			}

			programCounter += 4

		case CHANGE_BASE:
			relativeBase += val1
			programCounter += 2

		default:
			log.Fatalf("Got invalid instruction at %d: %d", programCounter, inputs[programCounter])
		}
	}

	// fmt.Println(inputs[0])
	// fmt.Println()
}

func decode(instruction int64) (int64, AddressMode, AddressMode, AddressMode) {
	opcode := instruction % 100
	first := AddressMode(0)
	second := AddressMode(0)
	third := AddressMode(0)

	instruction = instruction / 100
	if instruction >= 100 {
		third = AddressMode(instruction / 100)
		for instruction >= 100 {
			instruction -= 100
		}
	}

	if instruction >= 10 {
		second = AddressMode(instruction / 10)
		for instruction >= 10 {
			instruction -= 10
		}
	}

	if instruction >= 1 {
		first = AddressMode(instruction)
		for instruction >= 1 {
			instruction -= 1
		}
	}

	return opcode, first, second, third
}

func readInput() map[int]int64 {
	raw, err := ioutil.ReadFile("input.txt")
	if err != nil {
		log.Fatal("Could not read input, ", err)
	}

	line := strings.Split(string(raw), "\n")[0]
	pieces := strings.Split(line, ",")

	ints := make(map[int]int64, len(pieces))
	for i, s := range pieces {
		ints[i], err = strconv.ParseInt(s, 10, 64)
		if err != nil {
			log.Fatal("Could not parse %d: %s", i, s)
		}
	}

	return ints
}
