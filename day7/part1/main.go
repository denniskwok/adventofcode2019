package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strconv"
	"strings"
)

const (
	ADD           = 1
	MUL           = 2
	INPUT         = 3
	OUTPUT        = 4
	JUMP_IF_TRUE  = 5
	JUMP_IF_FALSE = 6
	LESS_THAN     = 7
	EQUAL         = 8
	END           = 99
)

type PhaseInput []int

func main() {
	inputs := readInput()
	phases := generatePhases()

	solve([]int{3, 15, 3, 16, 1002, 16, 10, 16, 1, 16, 15, 15, 4, 15, 99, 0, 0}, []PhaseInput{[]int{4, 3, 2, 1, 0}})
	solve([]int{3, 15, 3, 16, 1002, 16, 10, 16, 1, 16, 15, 15, 4, 15, 99, 0, 0}, phases)
	solve([]int{3, 23, 3, 24, 1002, 24, 10, 24, 1002, 23, -1, 23,
		101, 5, 23, 23, 1, 24, 23, 23, 4, 23, 99, 0, 0}, phases)
	solve([]int{3, 31, 3, 32, 1002, 32, 10, 32, 1001, 31, -2, 31, 1007, 31, 0, 33,
		1002, 33, 7, 33, 1, 33, 31, 31, 1, 32, 31, 31, 4, 31, 99, 0, 0, 0}, phases)

	solve(inputs, phases)
}

func solve(inputs []int, phases []PhaseInput) {

	maxOutput := -1

	for _, phase := range phases {
		lastOutput := 0
		for _, phaseInput := range phase {
			runInput := make([]int, len(inputs))
			copy(runInput, inputs)
			lastOutput = process(runInput, phaseInput, lastOutput)
		}
		if maxOutput < lastOutput {
			maxOutput = lastOutput
		}
	}

	fmt.Printf("Max thrust: %d\n", maxOutput)
}

// this is bad, should use a set
func generatePhases() []PhaseInput {
	const n = 5
	phases := make([]PhaseInput, 0)

	for a := 0; a < n; a++ {
		for b := 0; b < n; b++ {
			if a == b {
				continue
			}

			for c := 0; c < n; c++ {
				if c == a || c == b {
					continue
				}

				for d := 0; d < n; d++ {
					if d == a || d == b || d == c {
						continue
					}

					for e := 0; e < n; e++ {
						if e == a || e == b || e == c || e == d {
							continue
						}
						phase := make([]int, 5)
						phase[0] = a
						phase[1] = b
						phase[2] = c
						phase[3] = d
						phase[4] = e
						phases = append(phases, phase)
					}
				}
			}
		}
	}

	return phases
}

func process(inputs []int, phase int, signal int) int {
	// fmt.Printf("Phase %d Signal %d\n", phase, signal)
	sentPhase := false
	programCounter := 0
	lastOutput := 0
	for inputs[programCounter] != END {
		opcode, immediateFirst, immediateSecond, _ := decode(inputs[programCounter])
		// fmt.Printf("PC: %d INST: %d opcode %d %v %v %v\n", programCounter, inputs[programCounter], opcode, immediateFirst, immediateSecond, false)
		if opcode == END {
			break
		}

		op1 := inputs[programCounter+1]

		val1 := op1

		if !immediateFirst {
			val1 = inputs[op1]
		}

		switch opcode {
		case ADD:
			op2 := inputs[programCounter+2]
			op3 := inputs[programCounter+3]

			val2 := op2
			val3 := op3

			if !immediateSecond {
				val2 = inputs[op2]
			}

			// fmt.Printf("ADD %d %d %d\n", val1, val2, val3)
			inputs[val3] = val1 + val2

			programCounter += 4
		case MUL:
			op2 := inputs[programCounter+2]
			op3 := inputs[programCounter+3]

			val2 := op2
			val3 := op3

			if !immediateSecond {
				val2 = inputs[op2]
			}

			// fmt.Printf("MUL %d %d %d\n", val1, val2, val3)
			inputs[val3] = val1 * val2

			programCounter += 4

		case INPUT:
			val := phase
			if sentPhase {
				val = signal
			}
			sentPhase = true
			inputs[op1] = val

			programCounter += 2

		case OUTPUT:
			val := inputs[op1]

			lastOutput = val
			// fmt.Println(val)

			programCounter += 2

		case JUMP_IF_TRUE:
			if val1 != 0 {
				op2 := inputs[programCounter+2]
				val2 := op2

				if !immediateSecond {
					val2 = inputs[op2]
				}

				programCounter = val2
			} else {
				programCounter += 3
			}

		case JUMP_IF_FALSE:
			if val1 == 0 {
				op2 := inputs[programCounter+2]
				val2 := op2

				if !immediateSecond {
					val2 = inputs[op2]
				}

				programCounter = val2
			} else {
				programCounter += 3
			}

		case LESS_THAN:
			op2 := inputs[programCounter+2]
			op3 := inputs[programCounter+3]

			val2 := op2
			val3 := op3

			if !immediateSecond {
				val2 = inputs[op2]
			}

			if val1 < val2 {
				inputs[val3] = 1
			} else {
				inputs[val3] = 0
			}

			programCounter += 4

		case EQUAL:
			op2 := inputs[programCounter+2]
			op3 := inputs[programCounter+3]

			val2 := op2
			val3 := op3

			if !immediateSecond {
				val2 = inputs[op2]
			}

			if val1 == val2 {
				inputs[val3] = 1
			} else {
				inputs[val3] = 0
			}

			programCounter += 4

		default:
			log.Fatal("Got invalid instruction at %d: %d", programCounter, inputs[programCounter])
		}
		// fmt.Println(inputs)
	}
	// fmt.Println(lastOutput)

	return lastOutput
}

func decode(instruction int) (int, bool, bool, bool) {
	opcode := instruction % 100
	first := false
	second := false
	third := false

	instruction = instruction / 100
	if instruction >= 100 {
		third = true
		instruction -= 100
	}

	if instruction >= 10 {
		second = true
		instruction -= 10
	}

	if instruction >= 1 {
		first = true
		instruction -= 1
	}

	return opcode, first, second, third
}

func readInput() []int {
	raw, err := ioutil.ReadFile("input.txt")
	if err != nil {
		log.Fatal("Could not read input, ", err)
	}

	line := strings.Split(string(raw), "\n")[0]
	pieces := strings.Split(line, ",")

	ints := make([]int, len(pieces))
	for i, s := range pieces {
		ints[i], err = strconv.Atoi(s)
		if err != nil {
			log.Fatal("Could not parse %d: %s", i, s)
		}
	}

	return ints
}
