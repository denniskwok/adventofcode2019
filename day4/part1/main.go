package main

import (
	"fmt"
	"strconv"
)

func main() {
	fmt.Printf("%d - %v\n", 111111, meetsRules(111111, 0, 1000000))
	fmt.Printf("%d - %v\n", 223450, meetsRules(223450, 0, 1000000))
	fmt.Printf("%d - %v\n", 123789, meetsRules(123789, 0, 1000000))

	possiblePasswords := 0
	min := 172930
	max := 683082
	for i := min; i <= max; i++ {
		if meetsRules(i, min, max) {
			possiblePasswords++
		}
	}

	fmt.Println(possiblePasswords)
}

func meetsRules(number int, min int, max int) bool {
	if number < min || max < number {
		return false
	}

	// check double present
	s := strconv.Itoa(number)

	foundDouble := false
	for i := 0; i < len(s)-1; i++ {
		if s[i] == s[i+1] {
			foundDouble = true
		}

		current := int(s[i])
		next := int(s[i+1])

		if current > next {
			return false
		}
	}

	return foundDouble
}
