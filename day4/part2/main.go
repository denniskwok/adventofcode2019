package main

import (
	"fmt"
	"strconv"
)

func main() {
	fmt.Printf("%d - %v\n", 112233, meetsRules(112233, 0, 1000000))
	fmt.Printf("%d - %v\n", 123444, meetsRules(123444, 0, 1000000))
	fmt.Printf("%d - %v\n", 111122, meetsRules(111122, 0, 1000000))

	possiblePasswords := 0
	min := 172930
	max := 683082
	for i := min; i <= max; i++ {
		if meetsRules(i, min, max) {
			possiblePasswords++
		}
	}

	fmt.Println(possiblePasswords)
}

func meetsRules(number int, min int, max int) bool {
	if number < min || max < number {
		return false
	}

	// check double present
	s := strconv.Itoa(number)

	current := s[0]
	foundDouble := false
	numConseq := 1
	for i := 1; i < len(s); i++ {
		if current == s[i] {
			numConseq++
		} else {
			// switching
			if numConseq == 2 {
				foundDouble = true
			}
			current = s[i]
			numConseq = 1
		}
	}

	if numConseq == 2 {
		foundDouble = true
	}

	for i := 0; i < len(s)-1; i++ {
		current := int(s[i])
		next := int(s[i+1])

		if current > next {
			return false
		}
	}

	return foundDouble
}
