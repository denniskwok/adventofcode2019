package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strings"
)

type OrbitObject struct {
	Name   string
	Orbits *OrbitObject
}

func main() {
	orbitObjects := readInput()
	directOrbits := calcDirectOrbits(orbitObjects)
	indirectOrbits := calcIndirectOrbits(orbitObjects)

	fmt.Printf("Direct: %d Indirect %d Total %d\n", directOrbits, indirectOrbits, directOrbits+indirectOrbits)
}

func calcDirectOrbits(objects map[string]*OrbitObject) int {
	total := 0
	for _, obj := range objects {
		if obj.Orbits != nil {
			total++
		}
	}
	return total
}

func calcIndirectOrbits(objects map[string]*OrbitObject) int {
	total := 0

	for _, obj := range objects {
		if obj.Orbits == nil {
			continue
		}

		indirect := obj.Orbits
		for indirect.Orbits != nil {
			indirect = indirect.Orbits
			total++
		}
	}

	return total
}

func readInput() map[string]*OrbitObject {
	data, err := ioutil.ReadFile("input.txt")
	if err != nil {
		log.Fatal(err)
	}

	dataStrings := strings.Split(string(data), "\n")
	objects := make(map[string]*OrbitObject, len(dataStrings))

	for _, line := range dataStrings {
		if line == "" {
			continue
		}

		split := strings.Split(line, ")")
		if len(split) != 2 {
			log.Fatalf("Parsing error on %s", line)
		}

		name := split[0]
		orbiteeName := split[1]

		var orbit *OrbitObject
		var orbitee *OrbitObject
		found := false

		// create main object if missing
		if orbit, found = objects[name]; !found {
			object := &OrbitObject{
				Name:   name,
				Orbits: nil,
			}

			objects[name] = object
			orbit = object
		}

		orbitee, found = objects[orbiteeName]
		if !found {
			orbitee = &OrbitObject{
				Name:   orbiteeName,
				Orbits: orbit,
			}
			objects[orbiteeName] = orbitee
		} else {
			orbitee.Orbits = orbit
		}
	}

	return objects
}
